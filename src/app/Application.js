import config from '../config';
import EventEmitter from 'eventemitter3';
import StarWarsUniverse from './custom/StarWarsUniverse.js';
import Entity from './custom/Entity.js';

const EVENTS = {
    APP_READY: 'app_ready',
};

/**
 * App entry point.
 * All configurations are described in src/config.js
 */
export default class Application extends EventEmitter {
    constructor() {
        super();

        this.config = config;
        this.data = {};

        this.init();
    }

    static get events() {
        return EVENTS;
    }

    /**
     * Initializes the app.
     * Called when the DOM has loaded. You can initiate your custom classes here
     * and manipulate the DOM tree. Task data should be assigned to Application.data.
     * The APP_READY event should be emitted at the end of this method.
     */
    async init() {
        const starWars = new StarWarsUniverse();

        starWars.init();
        const resp = await starWars.init();
        const data = await resp.json();
        console.log(data);

        for (const [key, value] of Object.entries(data)) {

            let ent = await fetch(value);
            let dat = await ent.json();

            console.log(dat.count);

            let newEntity = new Entity(key);
            newEntity.data.count = dat.count;
            starWars.entities.push(newEntity);
        }
        // Initiate classes and wait for async operations here.

        this.data.universe = starWars;

        this.emit(Application.events.APP_READY);
    }
}